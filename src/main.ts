import * as express from 'express';
import * as http from 'http';
import { AddressInfo } from 'net';
import * as WebSocket from 'ws';

/**
 * Some predefined delay values (in milliseconds).
 */
export enum Delays {
  Short = 500,
  Medium = 2000,
  Long = 5000,
}

/**
 * Returns a Promise<string> that resolves after a given time.
 *
 * @param {string} name - A name.
 * @param {number=} [delay=Delays.Medium] - A number of milliseconds to delay resolution of the Promise.
 * @returns {Promise<string>}
 */
function delayedHello(
  name: string,
  delay: number = Delays.Medium,
): Promise<string> {
  return new Promise((resolve: (value?: string) => void) =>
    setTimeout(() => resolve(`Hello, ${name}`), delay),
  );
}

// Below are examples of using ESLint errors suppression
// Here it is suppressing a missing return type definition for the greeter function.

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export async function greeter(name: string) {
  return await delayedHello(name, Delays.Long);
}


const NUM_FEATURES = 5000; // Total number of (random) features
const NUM_FEATURES_UPDATE = 1000; // Number of features to be updated every interval
const UPDATE_INTERVAL = 300; // Interval duration in ms.

let j = 0;
const features = [];
while (j < NUM_FEATURES) {
  const coords = [
    // WGS84
    Math.random() * 4 + 3,
    Math.random() * 4 + 50
    // Web Mercator
    // Math.random() * 445278 + 333958,
    // Math.random() * 723881 + 6446275
  ];
  features.push({
    'type': 'Feature',
    'geometry': {
      'type': 'Point',
      'coordinates': coords
    },
    'properties': {
      'name': "#" + j.toString()
    }
  });
  j++;
}


const app = express();

//initialize a simple http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws: WebSocket) => {

  //connection is up, let's add a simple simple event
  ws.on('message', (message: string) => {

    //log the received message and send it back to the client
    console.log('received: %s', message);

    if (message == 'allFeatures') {
      ws.send(JSON.stringify({
        'message': 'allFeatures',
        'features': features
      }));
    }
  });

  //send immediatly a feedback to the incoming connection
  console.log('Sending all features');
  ws.send(JSON.stringify({
    'message': 'allFeatures',
    'features': {
      'type': 'FeatureCollection',
      'features': features
    }
  }));
});

wss.on('close', () => {
  clearInterval(interval);
});

let i = 0;
const interval = setInterval(() => {
  console.log('interval - # of clients', wss.clients.size);

  const updatedFeatures = [];
  const randomIndices: Array<number> = [];
  while (randomIndices.length < NUM_FEATURES_UPDATE) {
    const k = Math.floor(Math.random() * features.length);
    if (randomIndices.indexOf(k) === -1) randomIndices.push(k);
  }
  randomIndices.forEach((index) => {
    const SHIFT = 0.01; // WGS84
    // const SHIFT = 1500; // Web Mercator
    features[index].geometry.coordinates[0] += Math.random() * SHIFT - (.5 * SHIFT);
    features[index].geometry.coordinates[0] += Math.random() * SHIFT - (.5 * SHIFT);

    updatedFeatures.push(features[index]);
  })

  wss.clients.forEach(client => {
    console.log('sending updated features', i);
    client.send(JSON.stringify({
      'message': 'updatedFeatures',
      'features': {
        'type': 'FeatureCollection',
        'features': updatedFeatures
      }
    }));
  });
  i++;
}, UPDATE_INTERVAL);

//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${(server.address() as AddressInfo).port} :)`);
});
