# Geo Streaming server

[![TypeScript version][ts-badge]][typescript-4-5]
[![Node.js version][nodejs-badge]][nodejs]
[![APLv2][license-badge]][license]
[![Build Status - GitHub Actions][gha-badge]][gha-ci]

TODO

---------

## Bronnen
* [Node.js can HTTP/2 push!](https://medium.com/the-node-js-collection/node-js-can-http-2-push-b491894e1bb1)
* [Web Push Notification Server (github)](https://github.com/pmagaz/webpush-notification-server)
* [Creating a NodeJS Push Notification System with Service Workers](https://fjolt.com/article/javascript-notification-system-service-workers)
* [How do I build a simple node js server that sends and receives to a client](https://stackoverflow.com/questions/20869215/how-do-i-build-a-simple-node-js-server-that-sends-and-receives-to-a-client?rq=1)
* [How To Create a Web Server in Node.js with the HTTP Module](https://www.digitalocean.com/community/tutorials/how-to-create-a-web-server-in-node-js-with-the-http-module)
* [Server Push](https://blog.risingstack.com/node-js-http-2-push/)
* [Getting Started with Push Notifications in Node.js using Service Workers](https://www.section.io/engineering-education/push-notification-in-nodejs-using-service-worker/#generate-vapid-keys)
* [Web-push Notification using React and Node js](https://dev.to/ronyfr3/web-push-notification-using-react-and-node-js-oc9)

---------

# Sources
* [nationaalgeoregister free (hectometerpaal)](https://geodata.nationaalgeoregister.nl/locatieserver/v4/free/?q=type%3Ahectometerpaal&rows=50&fq=*)

---------

[ts-badge]: https://img.shields.io/badge/TypeScript-4.5-blue.svg
[nodejs-badge]: https://img.shields.io/badge/Node.js->=%2016.13-blue.svg
[nodejs]: https://nodejs.org/dist/latest-v14.x/docs/api/
[gha-badge]: https://github.com/jsynowiec/node-typescript-boilerplate/actions/workflows/nodejs.yml/badge.svg
[gha-ci]: https://github.com/jsynowiec/node-typescript-boilerplate/actions/workflows/nodejs.yml
[typescript]: https://www.typescriptlang.org/
[typescript-4-5]: https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-5.html
[license-badge]: https://img.shields.io/badge/license-APLv2-blue.svg
[license]: https://github.com/jsynowiec/node-typescript-boilerplate/blob/main/LICENSE
[sponsor-badge]: https://img.shields.io/badge/♥-Sponsor-fc0fb5.svg
[sponsor]: https://github.com/sponsors/jsynowiec
[jest]: https://facebook.github.io/jest/
[eslint]: https://github.com/eslint/eslint
[wiki-js-tests]: https://github.com/jsynowiec/node-typescript-boilerplate/wiki/Unit-tests-in-plain-JavaScript
[prettier]: https://prettier.io
[volta]: https://volta.sh
[volta-getting-started]: https://docs.volta.sh/guide/getting-started
[volta-tomdale]: https://twitter.com/tomdale/status/1162017336699838467?s=20
[gh-actions]: https://github.com/features/actions
[repo-template-action]: https://github.com/jsynowiec/node-typescript-boilerplate/generate